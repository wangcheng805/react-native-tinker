# 人人贷ReactNative热更新方案


## 系统结构和模块功能

ReactNative 热更新方案主要有以下4部分组成 ：

* RN代码 ： 下文中RN代码如无特殊说明均指业务代码，而不是ReactNative源码。
* 后台管理系统
* 热更新服务
* Native热更新SDK

下图从RN代码的发布流程、增量热更新流程描述方案中各个模块的职责

![react-native-patch模块流程图](./image/rn_patch_whole_process.png)

RN代码是业务层面代码，仅是一个输入源，下文将重点分析其他三个模块的详细职责和实现原理。

### 后台系统

这里主要介绍后台系统的功能，具体实现代码和数据库设计大家可以参考[这里](https://github.com/rrd-fe/tinker-system)

* 用户权限管理

和大部分后台系统没有区别，主要是对管理员、普通用户的管理，以及APP发布、操作权限管理

* RN代码发布

输入源代码或者编译后代码、App版本信息等生成相应RN版本的全量包、增量包等

* RN版本管理

管理已发布代码版本如：是否开发下载、是否使用增量下载、是否进行小流量测试等控制

* 提供查询、下载RN代码接口

后台系统提供查询、下载RN代码接口，主要是给内部流程如CI/CD、App打包集成最新RN代码使用。对外客户端SDK并不调用后台系统的接口

* 关于增量包的生成？

增量更新相关请参考[FAQ](https://github.com/rrd-fe/react-native-tinker/blob/master/docs/FAQ.md#%E5%85%B3%E4%BA%8E%E5%A2%9E%E9%87%8F%E6%9B%B4%E6%96%B0)增量更新的介绍

### 热更新服务

热更新服务功能很简单：给App提供检查增量更新接口，以及增量包和全量包下载接口。下面举个栗子说明热更新服务的request、response

* request url

App 启动时SDK会发送类似请求：rn/checkUpdate?appKey=ff14644b-1ecf-4175-b6ee-153bf7dd&clientVersion=50710&rnVersion=1

        appKey ： 和CodePush的类似，唯一代表App对应的RN更新仓库的字符串
        clientVersion ： 客户端版本
        rnVersion ： App当前使用的RN代码版本

* response

        {
            "status" : 0,
            "message" : "需要更新RN到最新版本5"
            "data" : {
                "needUpdate":true,
                "updateRNVersion":5,
                "fullPackageMd5":"9b393a51cbbd18ff85c25f3603863218","fullPackageUrl":"/430f9783-9587-46ce-b1f1-dea4f8ebac2d.zip","patchMd5":"3641b35e2315ee3fcd0860c989b0eaba","patchUrl":"/2b7b74aa-d859-40f7-98a4-d5a89cedd439.zip"
            }
        }

        主要返回是否需要更新、以及最新的全量包和增量包下载地址和他们的MD5，Native SDK根据response下载更新。

* 关于热更新服务的小流量实现

在实际开发中我们经常会有以下这些Case：

1. 希望上线内容先对内网生效验证通过后再开放所有用户
2. 希望上线内容只针对某些特殊用户开放
3. 或者能够定时如8：00对用户可见

我们以针对内网IP开放更新为例简单说明如何实现改需求 ：

在后台系统RN版本发布时预留了ABTest字段，可以如下配置 ：

        :::javascript
        {
            "allowIps" : ["172.0.0.1", "x.x.x.x"] //内网IP列表也可以找运维或者IT同事
        }

热更新服务可以如下实现 ：

        :::javascript
        //node.js版本的热更新服务，基于koa的伪代码
        let reloadData = db.getLastestRN();
        let clientIp = ctx.request.ip;
        if(reloadData.abtest.allowIps && reloadData.abtest.ip.includes(clientIp)){
            return reloadData;
        }else{
            return {
                "status" : 0,
                "message" : "没有更新内容"
                "data" : {
                    "needUpdate" : false,
                }
            }
        }


热更新服务可以根据团队情况采用node.js、Java、PHP等多种语言都可以，我们提供了[node.js版](https://github.com/rrd-fe/react-native-tinker-node-api-demo)供大家参考。

### 客户端SDK

经过我们的实践发现客户端SDK只需要三个功能(对应SDK会提供三个函数)就能够满足热更新需求。分别是 ：

* 检查、下载最新版本RN代码 (checkAndDownload)
* 重新加载最新的bundle (reload)
* 当RN热更新或者RN代码发生错误的时候回滚上一个版本 (rollback)

下面我们就挨个介绍这三个功能的详细实现

#### 检查更新下载流程

![react-native-tinker检查更新流程图](./image/rn_patch_download.png)

* 检查更新的时机

SDK只是提供了检查和下载更新的方法，具体检查调用时机由SDK的集成使用者来决定。例如在人人贷理财我们设定了启动App以及App从后台返回前台的时候进行检查。当然你也可以添加一个定时任务检查更新。

* 增量更新的merge

增量更新在上一篇文章中，已有详细的介绍，可以参考这里。

* Bundle的MD5校验

为了保证合并后的bundle是正确的，我们需要将合并后的bundle md5和更新接口返回的正确bundle md5进行对比，md5校验通过才认为检查下载流程成功。

#### 重新加载bundle流程

![react-native-patch 重新加载bundle流程图](./image/rn_patch_reload.png)

* 如何实现bundle的reload

关于Bundle的reload网上已经有很多文章这里就不详细说明了，简单贴一下示例代码：

        //ios示例 通过RCTBridge的reload方法重加载
        rctBridge = [[RCTBridge alloc] initWithBundleURL:rnCodeLocation
                                          moduleProvider:nil
                                           launchOptions:launchOptions];
        [rctBridge setValue:latestUrl forKey:@"bundleURL"];
        [rctBridge reload];

        //Android 通过ReactInstanceManager的recreateReactContextInBackground重新加载执行
        bundleLoaderField = reactInstanceManager.javaClass.getDeclaredField("mBundleLoader")
        bundleLoaderField.isAccessible = true
        bundleLoaderField.set(reactInstanceManager, latestJSBundleLoader)
        reactInstanceManager.recreateReactContextInBackground()

关于RCTBridge和ReactInstanceManager这里多说一点，这两个类在RN高阶开发中非常重要。不仅是RN实现热更新的核心类，RN Bundle 预加载和拆分多bundle等方案都依赖这两个类。

RN预加载：为了解决首次打开RN页面，bundle加载、执行JS代码时间过长导致明显白屏的问题，可以将bundle的加载放到App启动时，原理其实就是预生成这两个类的实例。

RN拆多Bundle：对于大、中型互联网公司通常一个App承担了多个业务入口，为了避免业务之间相关影响（A业务有bug导致整个App都无法使用）拆分多Bundle就非常有必要了，而多bundle其实就是要做这两个类或关联类的多实例管理。

上述两个问题后续我们会在单独文章详细介绍，敬请关注。

* bundle reload的时机

和检查更新一样，SDK只提供了reload方法，具体调用时机会交给SDK的使用者(业务开发方)来控制。通常我们是在下载完成并且当前页面不是RN页面或者从RN页面返回Native页面时调用reload。在RN页面reload会导致页面突然刷新，用户体验较差，当然如果需要修复紧急Bug可以选择发布强制刷新版本。

#### rollback流程

![react-native-patch RN版本回滚流程图](./image/rn_patch_rollback.png)

通常会在RN代码中捕获全局错误，然后调用rollback方法回滚到上一个版本（当然首先需要通过nativeModule将sdk的rollback暴露给RN），实例代码如下 ：

        :::javascript
        ErrorUtils.setGlobalHandler( (error, isFatal)=> {
            if(isFatal){
                error("App is crash, start rollback!");
                NativeModule.rollback()
            }
            log("App happend error, " + error.stack);
        })

RN代码捕获全局异常错误[参考这里](http://bbs.reactnative.cn/topic/627/%E9%94%99%E8%AF%AF%E6%8D%95%E6%8D%89%E4%B8%8E%E5%A4%84%E7%90%86)
