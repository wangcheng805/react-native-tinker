
* 关于苹果的政策

苹果App允许使用热更新[Apple's developer agreement](https://developer.apple.com/programs/information/Apple_Developer_Program_Information_8_12_15.pdf), 为了不影响用户体验，规定必须使用静默更新。


### 关于增量更新？

* 什么是增量更新

ReactNative打包后会产出以下两部分：

js bundle文件：RN业务代码，RN框架源码及依赖的第三方组件库等js代码合集
assets目录：RN代码中使用的图片资源

增量更新是指每次只下载内置版本相比最新版本变化部分，而不是全部下载。

assets目录下的文件比较容易处理，计算每一个资源的md5，找出新版本增加的md5对应的文件就是本次新增或修改资源列表。

js bundle的增量是只把代码改动的部分生成一个patch文件，App下载patch文件后和本地的js bundle merge操作就可以得到完整的最新代码。

目前常见的diff工具有bsdiff、google-diff-match-patch，经过对比我们最终选用了bsdiff，效率更高。

google-diff-match-patch的使用可以参考[这篇文章](https://blog.csdn.net/u013718120/article/details/55096393)

* 如何实现增量更新

增量更新实现步骤：

1. 后台系统生成增量包发布：例如发布V5版本会遍历所有低版本生成5-1.zip、5-2.zip、5-3.zip、5-4.zip增量包，同时还会生成V5版本的全量包。

2. App下载增量包：更新服务根据App内置的RN版本V3返回对应的增量包5-3.zip下载

3. 增量包的合并：下载解压缩完成后，合并assets文件夹，bundle文件利用bsdiff patch方法合并完成增量更新

* 为什么要用增量更新

我们目前工程(RN页面20个左右)编译后全量包在4M左右，但实际每次版本业务代码的修改基本都在10K以内（大多数情况1K），增量更新可以大大减少下载量，提高热更新的成功率。

除此之外还有一个很重要的原因前文提过相比code-push、react-native-patch等JS更新接口更新时机较慢，我们希望用户在打开App时就能用到最新版，所以需要尽量保证在App打开期间（启动期间、后台返回前台期间）就完成下载操作，下载的更新包越小用户首次看到新版的概率就越大。
