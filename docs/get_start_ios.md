
## iOS接入指南

### 准备工作
react-native-tinker（以下简称RNTinker）放置于npm仓库中，所以请先安装npm。
我们都使用yarn进行npm包的操作，可以使用如下指令安装yarn
```
npm install -g yarn
```

RNTinker是依赖于React Native（RN）的，在RN的工程的根目录下（package.json所在目录）运行：
```
yarn add react-native-tinker
```
然后
```
yarn install
```
这样在```node_modules```目录中会出现```react-native-tinker```目录

### cocoapods集成

在Podfile中添加
```
pod 'ReactNativeTinker', :path => '/path/to/react-native-tinker'
```
然后运行
```
pod install
```

注意：由于react-native-tinker对React的依赖，Podfile中也必须有React，不然会编译不过

### 代码集成

首先添加RNTinkerDelegate和RNTinkerRequestDelegate

```
#import "RNTinker.h"

@interface YourClass <RNTinkerDelegate, RNTinkerRequestDelegate>
@end
```
然后在适当的地方（比如启动的时候）初始化RNTinker：
```
- (void)initRNTinker {
    // 这里是main.io.jsbunle的URL
    NSURL *bundleUrl = [[NSBundle  mainBundle] URLForResource:@"main.ios"  withExtension:@"jsbundle"  subdirectory:@"RNBundles"];
    [[RNTinker  sharedInstance] setupForBundleUrl:bundleUrl delegate:self  requestDelegate:self  reloadMode:RNTinkerReloadModeImmediate];
}
```
然后实现RNTinkerRequestDelegate。
这里考虑到每个应用使用的网络库和返回格式都不固定，所以将网络请求的责任交给SDK的使用者，SDK只处理包装好的数据结构。
网络请求的方法是必须实现的。
```
#pragma mark - RNTinkerRequestDelegate
- (void)RNTinkerRequestWithCompletion:(void (^)(RNTinkerModel *))completion {
    [YourNetworkClass yourCheckMethod:RNTINKER_APP_KEY  complete:^(RNTinkerModel *model) {
        completion(model);
    }];
}
```
接下来实现RNTinkerDelegate的方法，当然这些方法是optional的，请按需使用。
```
#pragma mark - RNTinkerDelegate

- (BOOL)beforeReload:(NSDictionary *)dict {
    return YES;
}

- (void)afterReload:(NSDictionary *)dict {}

- (BOOL)beforeDownload:(NSDictionary *)dict {
     return YES;
}

- (void)afterDownload:(NSDictionary *)dict {}

- (BOOL)beforeRollback:(NSDictionary *)dict {
    return YES;
}

- (void)afterRollback:(NSDictionary *)dict {}

- (void)clientVersionUpdated:(NSDictionary *)dict {}

@end
```

