
## react native tinker get start

react native tinker 系统是由以下四部分组成：

* [tinker-cli](https://github.com/rrd-fe/react-native-tinker-cli) : 热更新系统的发版命令行工具，配合tinker-system使用

* [tinker-system](https://github.com/rrd-fe/tinker-system) : 热更新后台系统，维护RN的离线打包、发版、版本灰度配置、数据统计等功能

* [tinker-api](https://github.com/rrd-fe/react-native-tinker-node-api-demo) : 热跟新服务接口，给客户端SDK提供更新查询和下载接口，本文已Node.js为例

* [tinker-sdk](https://github.com/rrd-fe/react-native-tinker) : 热更新客户端SDK，提供检查、下载、加载更新包等功能

可以参考[这里](./tinker_advance.md)了解各模块间的关系以及详细设计。


### 一、Demo试用



### 二、SDK集成

* iOS SDK集成[参考文档](./get_start_ios.md)
* Android SDK集成[参考文档](./get_start_android.md)

### 三、搭建后台系统和API服务


### 四、tinker-cli使用



### 、集成生产环境

* APP集成SDK

* 网关服务开发热更新接口功能
